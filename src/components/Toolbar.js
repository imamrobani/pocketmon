import React from 'react'
import { View, Text, Image, TouchableOpacity, StyleSheet } from 'react-native'
import Colors from '../constants/Colors'
import Fonts from '../constants/Fonts'
import DeviceInfo from 'react-native-device-info'
import icBack from '../images/ic_back.png'

const isIphoneX = DeviceInfo.hasNotch()

const Toolbar = ({ onBack, name, number }) => {
  return (
    <View style={styles.container}>
      <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <TouchableOpacity onPress={onBack}>
            <Image source={icBack} style={styles.icon} />
          </TouchableOpacity>
          <Text style={styles.title}>{name}</Text>
        </View>
        <View>
          <Text style={styles.number}>#{number}</Text>
        </View>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    ...Platform.select({
      ios: {
        height: isIphoneX ? 90 : 75
      },
      android: {
        height: 75
      }
    }),
    backgroundColor: Colors.grass,
    paddingHorizontal: 16,
    justifyContent: 'center',
    paddingTop: isIphoneX ? 24 : null
  },
  icon: {
    height: 24,
    width: 24,
    tintColor: Colors.white
  },
  title: {
    fontFamily: Fonts.ROBOTO_BOLD,
    color: Colors.white,
    fontSize: 20,
    marginLeft: 16
  },
  number: {
    fontFamily: Fonts.ROBOTO_BOLD,
    color: Colors.white,
    fontSize: 16
  }
})

export default Toolbar

