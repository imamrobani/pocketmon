import React from 'react'
import { View, Text, StyleSheet, Dimensions, Platform } from 'react-native'
import Colors from '../constants/Colors'
import Fonts from '../constants/Fonts'
import DeviceInfo from 'react-native-device-info'

const isIphoneX = DeviceInfo.hasNotch()

const ToolbarMain = () => {
  return (
    <View style={styles.container}>
      <Text style={styles.title} >Pokemon Guide</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    ...Platform.select({
      ios: {
        height: isIphoneX ? 90 : 75
      },
      android: {
        height: 75
      }
    }),
    backgroundColor: Colors.red,
    paddingHorizontal: 24,
    justifyContent: 'center',
    paddingTop: isIphoneX ? 24 : null
  },
  title: {
    fontFamily: Fonts.ROBOTO_BOLD,
    color: Colors.white,
    fontSize: 18
  }
})

export default ToolbarMain

