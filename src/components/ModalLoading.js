import React from 'react'
import { View, StyleSheet, Animated } from 'react-native'
import Colors from '../constants/Colors'
import Modal from 'react-native-modal'
import icLoading from '../images/ic_loading.png'

const ModalLoading = ({ visible, rotateData }) => {
  return (
    <Modal isVisible={visible}>
      <View style={styles.container}>
        <Animated.Image
          source={icLoading}
          style={[styles.icon, { transform: rotateData }]}
        />
      </View>
    </Modal>
  )
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flexDirection: 'column'
  },
  iconWrapper: {
    backgroundColor: 'rgba(52, 52, 52, 0.8)',
    // backgroundColor: Colors.background,
    height: 100,
    width: 100,
    borderRadius: 10,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around'
  },
  icon: {
    width: 100,
    height: 100
  }
})

export default ModalLoading