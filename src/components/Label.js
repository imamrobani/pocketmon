import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { color } from 'react-native-reanimated'
import Colors from '../constants/Colors'
import Fonts from '../constants/Fonts'

const Label = ({ label }) => {
  return <Text style={styles.text}>{label}</Text>
}

const styles = StyleSheet.create({
  text: {
    fontFamily: Fonts.ROBOTO_BOLD,
    color: Colors.numberColor,
    marginBottom: 8
  }
})

export default Label

