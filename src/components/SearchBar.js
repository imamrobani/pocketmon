import React from 'react'
import { View, TextInput, Image, StyleSheet } from 'react-native'
import Colors from '../constants/Colors'
import Fonts from '../constants/Fonts'
import icSearch from '../images/ic_search.png'

const SeacrBar = ({ value, onChangeText, onEndEditing }) => {
  return (
    <View style={styles.container}>
      <TextInput
        autoCorrect={false}
        style={styles.inputStyle}
        placeholder='Search pokemon'
        value={value}
        onChangeText={onChangeText}
        onEndEditing={onEndEditing}
      />
      <Image source={icSearch} style={styles.icon} />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.white,
    height: 50,
    borderRadius: 20,
    flexDirection: 'row',
    marginBottom: 8,
    marginHorizontal: 3,
    width: 210
  },
  icon: {
    // marginLeft: 24,
    // marginLeft: 8,
    marginRight: 16,
    tintColor: Colors.red,
    alignSelf: 'center',
  },
  inputStyle: {
    flex: 1,
    fontSize: 16,
    fontFamily: Fonts.ROBOTO_REGULAR,
    color: Colors.nameColor,
    marginLeft: 16
  },
})

export default SeacrBar