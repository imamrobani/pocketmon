import React from 'react'
import { View, StyleSheet } from 'react-native'
import Colors from '../constants/Colors'

const Line = () => {
  return <View style={styles.line} />
}

const styles = StyleSheet.create({
  line: {
    height: 2,
    backgroundColor: Colors.line,
    marginVertical: 16
  }
})

export default Line