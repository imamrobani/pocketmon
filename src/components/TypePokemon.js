import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import Colors from '../constants/Colors'
import Fonts from '../constants/Fonts'

const TypePokemon = ({ name }) => {
  const color = name === 'Grass' ? Colors.grass : name === 'Poison'
    ? Colors.poison : name === 'Fire' ? Colors.fire : name === 'Flying'
      ? Colors.flying : name === 'Water' ? Colors.water : name === 'Bug'
        ? Colors.bug : name === 'Electric' ? Colors.electric : name === 'Ground'
          ? Colors.ground : name === 'Fairy' ? Colors.fairy : name === 'Fighting'
            ? Colors.fighting : name === 'Psychic' ? Colors.psychic : name === 'Rock'
              ? Colors.rock : name === 'Steel' ? Colors.steel : name === 'Ghost'
                ? Colors.ghost : name === 'Dragon' ? Colors.dragon : name === 'Ice'
                  ? Colors.ice : Colors.normal

  return (
    <View style={[styles.container, { backgroundColor: color }]}>
      <Text style={styles.type}>{name}</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    width: 40,
    height: 14,
    borderRadius: 8,
    justifyContent: 'center',
    alignItems: 'center'
  },
  type: {
    fontFamily: Fonts.ROBOTO_BOLD,
    fontSize: 8,
    color: Colors.white
  }
})

export default TypePokemon