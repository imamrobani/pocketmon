import React from 'react'
import { View, StatusBar, Platform } from 'react-native'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator, TransitionPresets } from '@react-navigation/stack'
import Splash from '../screens/splash/Splash'
import Home from '../screens/home/Home'
import DetailPokemon from '../screens/detail/DetailPokemon'

const Stack = createStackNavigator()

function MyStack() {
  const options = {
    gestureEnabled: true, // If you want to swipe back like iOS on Android
    ...TransitionPresets.SlideFromRightIOS
  }

  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
        cardStyle: {
          backgroundColor: 'white'
        }
      }}
    >
      <Stack.Screen name="Splash" component={Splash} options={options} />
      <Stack.Screen name="Home" component={Home} options={options} />
      <Stack.Screen name="DetailPokemon" component={DetailPokemon} />
    </Stack.Navigator>
  )
}

export default function App() {
  return (
    <View style={{ flex: 1 }}>
      <StatusBar backgroundColor={'black'} barStyle={Platform.OS === 'ios' ? 'default' : 'light-content'} />
      <NavigationContainer >
        <MyStack />
      </NavigationContainer>
    </View>
  )
}