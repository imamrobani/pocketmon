import axios from 'axios'
import Networks from '../constants/Networks'

export default axios.create({
  baseURL: Networks.BASE_URL,
  headers: {
    'Accept': 'application/json'
  }
})