import { StyleSheet, Dimensions } from 'react-native'
import Colors from '../../constants/Colors'
import Fonts from '../../constants/Fonts'

const WIDTH = Dimensions.get('window').width

const Styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: -5,
    backgroundColor: Colors.background,
    borderTopRightRadius: 8,
    borderTopLeftRadius: 8,
    padding: 14
  },

  //style BoxContent
  containerBox: {
    backgroundColor: Colors.white,
    height: 135,
    borderRadius: 8,
    padding: 8,
    margin: 4,
    height: WIDTH / 3
  },
  numberText: {
    fontFamily: Fonts.ROBOTO_REGULAR,
    color: Colors.numberColor
  },
  containerContent: {
    justifyContent: 'center',
    // alignItems: 'center',
    paddingVertical: 4
  },
  image: {
    width: 48,
    height: 48
  },
  nameText: {
    fontFamily: Fonts.ROBOTO_REGULAR,
    color: Colors.nameColor,
    marginVertical: 4
  },
  containerTypes: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingHorizontal: Dimensions.get('window').width > 400 ? 4 : null
  },
  boxInvisible: {
    backgroundColor: 'transparent'
  }
})

export default Styles