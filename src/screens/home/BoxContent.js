import React from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'
import Styles from './Styles'
import TypePokemon from '../../components/TypePokemon'

const BoxContent = ({ onPress, number, image, name, types, empty }) => {
  const inVisible = empty ? Styles.boxInvisible : null
  const tag = !empty ? '#' : null

  return (
    <TouchableOpacity style={[Styles.containerBox, inVisible]} onPress={onPress}>
      <Text style={Styles.numberText}>{tag}{number}</Text>
      <View style={Styles.containerContent}>
        <View style={{ alignItems: 'center' }}>
          <Image
            style={Styles.image}
            source={{ uri: image }}
            resizeMode='contain'
          />
          <Text style={Styles.nameText}>{name}</Text>
        </View>
        <View style={Styles.containerTypes}>
          {types.map((item, index) => {
            return (
              <TypePokemon key={index} name={item} />
            )
          })}
        </View>
      </View>
    </TouchableOpacity>
  )
}

export default BoxContent