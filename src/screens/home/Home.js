import React, { useEffect, useState } from 'react'
import { View, FlatList, Animated, Easing } from 'react-native'
import Styles from './Styles'
import ToolbarMain from '../../components/ToolbarMain'
import SearchBar from '../../components/SearchBar'
import BoxContent from './BoxContent'
import ModalLoading from '../../components/ModalLoading'
import usePokemons from '../../hooks/usePokemons'

const Home = ({ navigation }) => {
  const [numColumns, pokemons, isLoading, searchPokemon] = usePokemons()
  const [rotateValueHolder, setRotateValueHolder] = useState(new Animated.Value(0))

  const startLoading = () => {
    rotateValueHolder.setValue(0)
    Animated.timing(rotateValueHolder, {
      useNativeDriver: true,
      toValue: 1,
      duration: 3000,
      easing: Easing.linear
    }).start((val) => {
      if (val.finished) {
        startLoading()
      }
    })
  }

  const RotateData = rotateValueHolder.interpolate({
    inputRange: [0, 1],
    outputRange: ['0deg', '360deg']
  })

  useEffect(() => {
    startLoading()
  }, [])

  return (
    <View style={{ flex: 1 }}>
      <ToolbarMain />
      <ModalLoading
        visible={isLoading}
        rotateData={[{ rotate: RotateData }]}
      />
      <View style={Styles.container}>
        <View style={{ alignItems: 'flex-end' }}>
          <SearchBar onChangeText={(text) => searchPokemon(text)} />
        </View>
        <FlatList
          showsVerticalScrollIndicator={false}
          data={pokemons}
          renderItem={({ item }) =>
            <View style={{ flex: 1 }}>
              <BoxContent
                number={item.number}
                image={item.image}
                name={item.name}
                types={item.types}
                onPress={() => navigation.navigate('DetailPokemon', {
                  id: item.id,
                  number: item.number,
                  name: item.name
                })}
                empty={item.empty}
              />
            </View>
          }
          keyExtractor={(item, index) => index.toString()}
          numColumns={numColumns}
          key={numColumns}
        />
      </View>
    </View>
  )
}

export default Home