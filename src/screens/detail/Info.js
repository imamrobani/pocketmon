import React from 'react'
import { View, Text } from 'react-native'
import Styles from './Styles'
import Label from '../../components/Label'
import Line from '../../components/Line'
import LinearGradient from 'react-native-linear-gradient'
import { createShimmerPlaceholder } from 'react-native-shimmer-placeholder'

const ShimmerPlaceHolder = createShimmerPlaceholder(LinearGradient)

const Info = ({ label, classification, visible }) => {
  return (
    <View>
      <ShimmerPlaceHolder visible={visible} shimmerStyle={Styles.textShimmerShort}>
        <Label label={label} />
      </ShimmerPlaceHolder>
      <ShimmerPlaceHolder visible={visible} shimmerStyle={Styles.textShimmer}>
        <Text style={Styles.infoText}>{classification}</Text>
      </ShimmerPlaceHolder>
      <Line />
    </View>
  )
}

export default Info