import { StyleSheet } from 'react-native'
import Colors from '../../constants/Colors'
import Fonts from '../../constants/Fonts'

const Styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: -5,
    backgroundColor: Colors.background,
    borderTopRightRadius: 8,
    borderTopLeftRadius: 8,
    padding: 16,
    // alignItems: 'center'
  },
  containerImage: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 16,
    marginBottom: 32
  },
  imageDetail: {
    width: 140,
    height: 140,
    borderRadius: 140 / 2
  },
  infoText: {
    fontFamily: Fonts.ROBOTO_REGULAR,
    color: Colors.nameColor,
    marginBottom: 16
  },

  //content Evolutions
  cardEvolution: {
    backgroundColor: Colors.evolution,
    borderRadius: 8,
    padding: 16,
    marginVertical: 8,
    flexDirection: 'row'
  },
  imageEvolution: {
    width: 72,
    height: 72,
    borderRadius: 72 / 2,
    marginRight: 16
  },
  nameEvolution: {
    fontFamily: Fonts.ROBOTO_REGULAR,
    color: Colors.nameColor,
    marginBottom: 8
  },

  /*style shimmer*/
  textShimmerShort: {
    marginBottom: 8,
    borderRadius: 5,
    height: 17,
    width: 100
  },
  textShimmer: {
    marginBottom: 16,
    borderRadius: 5,
    height: 17,
    width: 150
  },
  textShimmerLong: {
    marginBottom: 16,
    borderRadius: 5,
    height: 17,
  },
  cardShimmer: {
    borderRadius: 8,
    marginVertical: 8,
    height: 85,
    width: 320
  }
})

export default Styles