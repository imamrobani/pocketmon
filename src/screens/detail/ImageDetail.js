import React from 'react'
import { View, Image } from 'react-native'
import Styles from './Styles'
import LinearGradient from 'react-native-linear-gradient'
import { createShimmerPlaceholder } from 'react-native-shimmer-placeholder'

const ShimmerPlaceHolder = createShimmerPlaceholder(LinearGradient)

const ImageDetail = ({ source, visible }) => {
  return (
    <View style={Styles.containerImage}>
      <ShimmerPlaceHolder visible={visible} shimmerStyle={Styles.imageDetail}>
        <Image
          source={{ uri: source }}
          style={Styles.imageDetail}
          resizeMode='contain'
        />
      </ShimmerPlaceHolder>
    </View>
  )
}

export default ImageDetail