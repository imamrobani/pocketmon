import React from 'react'
import { View, Text, Image } from 'react-native'
import Label from '../../components/Label'
import Styles from './Styles'
import TypePokemon from '../../components/TypePokemon'
import LinearGradient from 'react-native-linear-gradient'
import { createShimmerPlaceholder } from 'react-native-shimmer-placeholder'

const ShimmerPlaceHolder = createShimmerPlaceholder(LinearGradient)

const Evolution = ({ data, visible }) => {
  if (!data) {
    return null
  }

  return (
    <View style={{ marginVertical: 16 }}>
      <ShimmerPlaceHolder visible={visible} shimmerStyle={Styles.textShimmerShort}>
        <Label label='Evolution' />
      </ShimmerPlaceHolder>
      <ShimmerPlaceHolder visible={visible} shimmerStyle={Styles.cardShimmer} />
      {data.map((item, index) => {
        return (
          <View key={index} style={Styles.cardEvolution}>
            <ShimmerPlaceHolder visible={visible} shimmerStyle={Styles.imageEvolution}>
              <Image
                source={{ uri: item.image }}
                style={Styles.imageEvolution}
              />
            </ShimmerPlaceHolder>
            <View>
              <Text style={Styles.nameEvolution}>#{item.number} - {item.name}</Text>
              <View style={{ flexDirection: 'row' }}>
                {item.types.map((item, index) => {
                  return (
                    <View key={index} style={{ marginRight: 8 }}>
                      <TypePokemon name={item} />
                    </View>
                  )
                })}
              </View>
            </View>
          </View>
        )
      })}
    </View>
  )
}

export default Evolution