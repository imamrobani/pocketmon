import React from 'react'
import { View, Text, FlatList } from 'react-native'
import Styles from './Styles'
import Label from '../../components/Label'
import Line from '../../components/Line'
import LinearGradient from 'react-native-linear-gradient'
import { createShimmerPlaceholder } from 'react-native-shimmer-placeholder'

const ShimmerPlaceHolder = createShimmerPlaceholder(LinearGradient)

const InfoArray = ({ label, data, visible }) => {
  return (
    <View>
      <ShimmerPlaceHolder visible={visible} shimmerStyle={Styles.textShimmerShort}>
        <Label label={label} />
      </ShimmerPlaceHolder>
      <ShimmerPlaceHolder visible={visible} shimmerStyle={Styles.textShimmerLong}>
        <FlatList
          horizontal={true}
          data={data}
          renderItem={({ item }) =>
            <Text style={Styles.infoText}>{item}</Text>
          }
          keyExtractor={(item, index) => index.toString()}
          ItemSeparatorComponent={() => <Text style={Styles.infoText}>, </Text>}
        />
      </ShimmerPlaceHolder>
      <Line />
    </View>
  )
}

export default InfoArray