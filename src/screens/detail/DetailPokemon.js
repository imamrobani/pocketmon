import React from 'react'
import { View, ScrollView } from 'react-native'
import Styles from './Styles'
import Toolbar from '../../components/Toolbar'
import ImageDetail from './ImageDetail'
import Info from './Info'
import InfoArray from './InfoArray'
import Evolution from './Evolution'
import usePokemon from '../../hooks/usePokemon'

const DetailPokemon = ({ navigation, route }) => {
  const [pokemon, name, number, isShimmer] = usePokemon({ route })

  return (
    <View style={{ flex: 1 }}>
      <Toolbar name={name} number={number} onBack={() => navigation.pop()} />
      <ScrollView showsVerticalScrollIndicator={false} style={Styles.container}>
        <ImageDetail visible={isShimmer} source={pokemon.image} />
        <InfoArray label='Types' visible={isShimmer} data={pokemon.types} />
        <View style={{ marginTop: 16 }}>
          <Info label='Classification' visible={isShimmer} classification={pokemon.classification} />
        </View>
        <View style={{ marginTop: 16 }}>
          <InfoArray label='Resistant' visible={isShimmer} data={pokemon.resistant} />
        </View>
        <Evolution visible={isShimmer} data={pokemon.evolutions} />
      </ScrollView>
    </View>
  )
}

export default DetailPokemon