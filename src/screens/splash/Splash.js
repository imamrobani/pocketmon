import React, { useEffect, useRef } from 'react'
import { Image, View, Animated } from 'react-native'
import { StackActions } from '@react-navigation/native'
import ic_pokemon from '../../images/ic_pokemon.png'

const Splash = (props) => {
  const fadeAnim = useRef(new Animated.Value(0)).current

  useEffect(() => {
    Animated.timing(
      fadeAnim,
      {
        toValue: 1,
        duration: 3000,
        useNativeDriver: true
      }
    ).start(async () => {
      props.navigation.dispatch(
        StackActions.replace('Home')
      )
    })
  }, [])

  return (
    <Animated.View style={{ opacity: fadeAnim, flex: 1 }}>
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Image source={ic_pokemon} />
      </View>
    </Animated.View>
  )
}

export default Splash