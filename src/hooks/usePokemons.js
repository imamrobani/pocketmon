import { Alert } from 'react-native'
import { useState, useEffect } from 'react'
import pokemonApi from '../api/pokemonApi'

export default () => {
  const [pokemons, setPokemons] = useState(null)
  const [isLoading, setIsLoading] = useState(true)
  const [result, setResults] = useState(null)
  const numColumns = 3
  const numList = 160

  const formatData = (pokemons, numColumns) => {
    const totalRows = Math.floor(pokemons.length / numColumns)
    let totalLastRow = pokemons.length - (totalRows * numColumns)

    while (totalLastRow !== 0 && totalLastRow !== numColumns) {
      pokemons.push({ id: '', number: '', name: '', image: null, types: [], empty: true, })
      totalLastRow++
    }

    setPokemons(pokemons)
    setIsLoading(false)
  }

  const getPokemons = async () => {
    const body = {
      query: `{
        pokemons(first:${numList}){
          id
          number
          name
          image
          types
        }
      }`
    }

    try {
      const response = await pokemonApi.post('', body)
      const pokemons = response.data.data.pokemons

      setResults(pokemons)
      formatData(pokemons, numColumns)
    } catch (err) {
      setIsLoading(false)
      Alert.alert('Opps ..', 'Something went wrong, try again later')
    }
  }

  const searchPokemon = text => {
    const newData = result.filter(item => item.name.match(text))
    formatData(newData, numColumns)
  }

  useEffect(() => {
    getPokemons()
  }, [])

  return [numColumns, pokemons, isLoading, searchPokemon]
}