import { Alert } from 'react-native'
import { useState, useEffect } from 'react'
import pokemonApi from '../api/pokemonApi'

export default ({ route }) => {
  const [pokemon, setPokemon] = useState({
    number: '',
    name: '',
    image: null,
    types: [],
    classification: '',
    resistant: [],
    evolutions: []
  })
  const [id, setId] = useState(route.params.id)
  const [number, setNumber] = useState(route.params.number)
  const [name, setName] = useState(route.params.name)
  const [isShimmer, setIsShimmer] = useState(false)

  const getDetail = async () => {
    const body = {
      query: `{
        pokemon(id:"${id}", name:"${name}"){
          number
          name
          image
          types
          classification
          resistant
          evolutions {
            number
            name
            image
            types
          }
        }
      }`
    }

    try {
      const response = await pokemonApi.post('', body)
      setPokemon(response.data.data.pokemon)
      setIsShimmer(true)
    } catch (err) {
      setIsShimmer(true)
      Alert.alert('Opps ..', 'Something went wrong, try again later')
    }
  }

  useEffect(() => {
    getDetail()
  }, [])

  return [pokemon, name, number, isShimmer]
}