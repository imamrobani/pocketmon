const Colors = {
  red: 'rgb(229, 28, 35)',
  background: 'rgb(244, 247, 250)',
  white: 'rgb(255, 255, 255)',
  numberColor: 'rgb(126, 148, 179)',
  nameColor: 'rgb(29, 57, 98)',
  line: 'rgb(234, 239, 247)',
  evolution: 'rgb(224, 232, 243)',

  //Colors Type Pokemons
  grass: 'rgb(139, 195, 74)',
  poison: 'rgb(170, 34, 176)',
  fire: 'rgb(255, 122, 0)',
  flying: 'rgb(204, 156, 247)',
  water: 'rgb(43, 195, 255)',
  bug: 'rgb(158, 181, 65)',
  normal: 'rgb(201, 197, 200)',
  electric: 'rgb(252, 210, 58)',
  ground: 'rgb(140, 110, 84)',
  fairy: 'rgb(250, 197, 243)',
  fighting: 'rgb(229, 28, 35)',
  psychic: 'rgb(235, 120, 179)',
  rock: 'rgb(184, 151, 123)',
  steel: 'rgb(173, 168, 172)',
  ghost: 'rgb(139, 27, 245)',
  ice: 'rgb(153, 236, 255)',
  dragon: 'rgb(90, 12, 245)'
}

export default Colors